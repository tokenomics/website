---
title: Call for paper
---

**Tokenomics 2020**

2nd International Conference on Blockchain Economics, Security and Protocols

11 and 12 May 2020, Toulouse, France

After the great success of [Tokenomics 2019](http://tokenomics2019.org/), we are pleased to announce [Tokenomics 2020](https://tokenomics.events/), the 2nd International Conference on Blockchain Economics, Security and Protocols. This year’s conference is hosted by Toulouse School of Economics (TSE).

**Tokenomics** is an international forum for theory, design, analysis, implementation and applications of blockchains and smart contracts. The goal of the conference is to bring together economists,  computer science researchers and practitioners working on blockchains in a unique program featuring outstanding invited talks and academic presentations. 

Keynote speakers will include



*   Jean Tirole, 2014 laureate of the Sveriges Riksbank prize in economic sciences in memory of Alfred Nobel, honorary chairman of the Jean-Jacques Laffont - Toulouse School of Economics Foundation and chairman of the Institute for Advanced Study in Toulouse,
*   Long Chen, Secretary-General of the Luohan Academy, an open research institute initiated by Alibaba, and former Chief Strategy Officer at Ant Financial,
*   Ittai Abraham, senior researcher at vmware research.


# **Topics of interest**

Topics of interest include, but are not limited to:



*   Distributed protocols for blockchains 
*   Lightweight protocols and networking issues of blockchains 
*   Fairness and Consistency (logical and economical) of blockchains 
*   Blockchain security, privacy and cryptographic tools 
*   Incentive theory, mechanism design, contract theory for blockchains 
*   Repeated games, collaborative games, reputation, algorithmic game theory 
*   ICO and cryptocurrencies, token valuation, governance and voting 
*   Trust models for blockchains 
*   Smart contracts and programming languages
*   Formal methods for blockchains 
*   Secure multiparty computations (SMPC, e.g., auctions with sealed bids) 
*   Multi-agent systems and machine learning techniques applied to blockchains 

The program committee encourages the submission of original interdisciplinary works exploring the conjunction of economic concerns with distributed systems, networks and system security. 


# **Submission guidelines**

The submission website is: [http://tokenomics2020.sciencesconf.org/](http://tokenomics2020.sciencesconf.org/)

Submissions can be made in the computer science track or the economics track under the following formats:



1. Computer Science track:
    * Regular papers: 15 pages in the LNCS format. A clearly marked
appendix is allowed (no more than 10 pages).
    * Brief announcements: 5 pages in LNCS format. A clearly marked
appendix is allowed (no more than 10 pages).
2. Economics Track: Free format.

Submissions should include authors’ names and affiliations, and a short abstract of the paper’s contribution.

All papers must be submitted electronically according to the instructions and forms found here and on the submission site. For each accepted paper the conference requires at least one registration. 

**Conference proceedings:**

Regular Papers accepted in the Computer Science track will be published
in the conference proceedings. Brief announcements accepted in the
Computer Science track will be published according to the authors'
wishes in the conference proceedings. They should describe novel, previously unpublished scientific contributions. They will be subject to peer review. Authors may submit only work that does not substantially overlap with work that is currently submitted or has been accepted for publication in a conference with proceedings or a journal. Papers should be formatted in LNCS format and submitted as PDF files. The authors must also sign the LNCS copyright form when submitting the final version.


# **Important dates**

Submission deadline: January 20th (extended), 2019

Acceptance notification: March 1st, 2020

Conference dates: May 11-12, 2020 


# **Award**

The Ethereum France - Kaiko Prize for Research in Cryptoeconomics will reward the best paper and talk.


# **Programme committee**


## **_Computer science:_**

Emmanuelle Anceaume, CNRS, Irisa (France)

Daniel Augot, INRIA, Ecole Polytechnique (France)

Quentin Bramas, ICUBE, University of Strasbourg (France)

Vincent Danos, CNRS, Ecole Normale Supérieure (France)

Giuseppe Antonio Di Luna, Sapienza University of Rome (Italy)

Antonio Fernández Anta, IMDEA Networks (Spain)

Fabrice Le Fessant, OCaml PRO (France)

Juan A. Garay, Texas A&M University (USA)

Chryssis Georgiou, University of Cyprus (Cyprus)

Vincent Gramoli, The University of Sydney (Australia)

Braham Hamid, IRIT (France)

Maurice Herlihy, Brown University (USA)

Pascal Lafourcade, Université Clermont Auvergne (France)

Mario Larangeira, IOHK, ​Tokyo Institute of Technology (Japan)

Romaric Ludinard, IMT Atlantique (France)

Maria Potop-Butucaru,  Sorbonne Université (France) 

Leonardo Querzoni, Sapienza University of Rome (Italy)

François Taiani, Université Rennes 1, Irisa (France)

Sara Tucci-Piergiovanni, CEA LIST (France)

Marko Vukolic, IBM Research - Zurich (Switzerland)

Josef Widder, Interchain Foundation & TU Wien (Austria)


## **_Economics:_**

Bruno Biais, HEC Paris (France)

Christophe Bisière, University Toulouse Capitole, TSE and TSM-R (France)

Matthieu Bouvard, University Toulouse Capitole, TSE and TSM-R  (France)

Catherine Casamatta, University Toulouse Capitole, TSE and TSM-R  (France)

Jonathan Chiu, Bank of Canada (Canada)

Will Cong, Cornell University, Johnson Graduate School of Management (USA)

Guillaume Haeringer, Baruch College, Zicklin School of Business (USA)

Hanna	Halaburda, New York University and Bank of Canada (USA & Canada)

Zhiguo He, University of Chicago, Booth School of Business (USA)

Emiliano Pagnotta, Imperial College Business School (U.K.)

Julien Pratt, CNRS and CREST (France)

Linda Shilling, Ecole Polytechnique and CREST (France)

Katrin Tinn, McGill University, Desautels Faculty of Management (Canada)

David Yermack, New York University, Stern School of Business (USA)


# **Conference organisers**

Emmanuelle Anceaume (CNRS, Irisa), Christophe Bisière (Toulouse School of Economics, University Toulouse Capitole, TSM-R), Quentin Bramas (ICUBE, University of Strasbourg), Matthieu Bouvard (Toulouse School of Economics, University Toulouse Capitole, TSM-R), Catherine Casamatta (Toulouse School of Economics, University Toulouse Capitole, TSM-R)


# **Sponsors**

![/sponsors.png](/sponsors.png)
