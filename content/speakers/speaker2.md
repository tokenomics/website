---
id: speaker2
name: Long Chen
company: Luohan Academy
feature: true
photoURL: /images/LongChen.png
socials:
shortBio: "From Wikipedia, the free encyclopedia: Chen Long (Chinese: 陈龙; pinyin: Chén Lóng) currently serves as the director of Luohan Academy, the open think tank of Alibaba. Before this, he was the chief strategy officer of Ant Financial Group since 2014. Chen received his Ph.D. in Finance from University of Toronto, and was a tenured professor at Olin Business School, Washington University in St. Louis. After returning to China in 2010, Chen took the position of the Associate Dean of Cheung Kong Graduate School of Business (CKGSB), Professor of Finance."
companyLogo: ''
country: ''
---
From Wikipedia, the free encyclopedia: Chen Long (Chinese: 陈龙; pinyin: Chén Lóng) currently serves as the director of Luohan Academy, the open think tank of Alibaba. Before this, he was the chief strategy officer of Ant Financial Group since 2014. Chen received his Ph.D. in Finance from University of Toronto, and was a tenured professor at Olin Business School, Washington University in St. Louis. After returning to China in 2010, Chen took the position of the Associate Dean of Cheung Kong Graduate School of Business (CKGSB), Professor of Finance.
