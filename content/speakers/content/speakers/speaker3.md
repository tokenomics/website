---
id: speaker3
name: Ittai Abraham
company: VMware Research Group
feature: true
photoURL: https://research.vmware.com/files/users/0/0/0/0/0/0/8/image001.png?v=2
socials: 
shortBio: "https://research.vmware.com/researchers/ittai-abraham"
companyLogo: ''
country: ''
---