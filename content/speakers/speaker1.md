---
id: speaker1
name: Jean Tirole
company: Toulouse School of Economics
feature: true
photoURL: https://pbs.twimg.com/profile_images/524113279589687298/O07AXQ7x_400x400.jpeg
socials:
  - icon: twitter
    link: 'https://twitter.com/jeantirole'
    name: 'Jean Tirole'
shortBio: "From Wikipedia, the free encyclopedia: Jean Tirole (born 9 August 1953) is a French professor of economics. He focuses on industrial organization, game theory, banking and finance, and economics and psychology. In 2014 he was awarded the Nobel Memorial Prize in Economic Sciences for his analysis of market power and regulation."
companyLogo: ''
country: ''
---
From Wikipedia, the free encyclopedia: Jean Tirole (born 9 August 1953) is a French professor of economics. He focuses on industrial organization, game theory, banking and finance, and economics and psychology. In 2014 he was awarded the Nobel Memorial Prize in Economic Sciences for his analysis of market power and regulation.
