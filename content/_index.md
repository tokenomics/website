---
title: "Home"
date: 2019-07-25T11:01:39Z
---

{{% jumbo img="/images/tse_building.jpg" imgLabel="TOKENOMICS" %}}


# TOKENOMICS
## 11-12 May, 2020
### Toulouse, France
#### In the new building of Toulouse School of Economics
<br/>
<a href="https://tokenomics2020.sciencesconf.org/resource/page/id/1" targe="_blank" class="submitbutton">Submit a Paper</a>
<style>
.submitbutton {
    background-color: #0000002e;
    border-radius: 28px;
    border: 1px solid #00000038;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-size:17px;
	padding:16px 31px;
	text-decoration:none;
	text-shadow:0px 1px 0px #2f6627;
}
.submitbutton:hover {
	background-color:transparent;
}
.submitbutton:active {
	position:relative;
	top:1px;
}
</style>
    
{{% /jumbo %}}


<section class="container fixed">
	<div class="">
    Tokenomics, International Conference on Blockchain Economics, Security and Protocols is an international forum for theory, design, analysis, implementation and applications of blockchains and smart contracts. Original interdisciplinary works exploring the conjunction of economic concerns with distributed systems, networks and system security are particularly encouraged.
    <br/><br/>
    The goal of the conference is to bring together economists, computer science researchers and practitioners working on blockchains in a unique program featuring outstanding invited talks and academic presentations.
    <br/><br/>
    <strong>Award</strong><br/>
    The Ethereum France - Kaiko Prize for Research in Cryptoeconomics will reward the best paper and talk.
    <br/>
    <br/>
    <strong style="text-decoration: underline;"><a target="_blank" href="https://tokenomics2020.sciencesconf.org/resource/page/id/1">Submission Guidelines</a></strong>
    <br/>
    <br/>
    <strong style="text-decoration: underline;"><a target="_blank" href="https://www.tse-fr.eu/conferences/2020-2nd-tokenomics-conference">TSE web site</a></strong>
    </div>
</section>

{{% home-info what="Submission Deadline: January 20 (extended), 2019; Acceptance notification: March 1, 2020" class="primary" %}}
## Important Dates:
{{% /home-info %}}


{{% home-speakers %}}
## Invited Speakers

{{% /home-speakers %}}



{{% chairs-and-committee %}}
{{% /chairs-and-committee %}}


{{% home-location
    image="/images/map.png"
    address="Toulouse"
    latitude="43.5976061"
    longitude="1.4385773" %}}

## The venue

The conference will take place in the new building of Toulouse School of Economics.

{{% /home-location %}}

{{% partners categories="platinium" %}}
## Partners
{{% /partners %}}



