## Chairs & Organisers
### General Chairs

- Emmanuelle Anceaume, *IRISA*
- Christophe Bisière, *University Toulouse Capitole, TSE and TSM-R*
- Quentin Bramas, *ICUBE, University of Strasbourg*
- Matthieu Bouvard, *University Toulouse Capitole, TSE and TSM-R*
- Catherine Casamatta, *University Toulouse Capitole, TSE and TSM-R*

### Local Organisers

- Christophe Bisière, *University Toulouse Capitole, TSE and TSM-R*
- Matthieu Bouvard, *University Toulouse Capitole, TSE and TSM-R*
- Catherine Casamatta, *University Toulouse Capitole, TSE and TSM-R*


### Steering Committee

- Vincent Danos, *Ecole Normale Supérieure*
- Maurice Herlihy, *Brown University*
- Maria Potop-Butucaru, *Sorbonne Université*
- Julien Prat, CREST, *Ecole Polytechnique*
- Sara Tucci-Piergiovanni, *CEA LIST*