## Program Committee

### Computer Science

- Emmanuelle Anceaume, _CNRS, Irisa (France)_
- Daniel Augot, _INRIA, Ecole Polytechnique (France)_
- Quentin Bramas, _ICUBE, University of Strasbourg (France)_
- Vincent Danos, _CNRS, Ecole Normale Supérieure (France)_
- Giuseppe Antonio Di Luna, _Sapienza University of Rome (Italy)_
- Antonio Fernández Anta, _IMDEA Networks (Spain)_
- Fabrice Le Fessant, _OCaml PRO (France)_
- Juan A. Garay, _Texas A&M University (USA)_
- Chryssis Georgiou, _University of Cyprus (Cyprus)_
- Vincent Gramoli, _The University of Sydney (Australia)_
- Braham Hamid, _IRIT (France)_
- Maurice Herlihy, _Brown University (USA)_
- Pascal Lafourcade, _Université Clermont Auvergne (France)_
- Mario Larangeira, _IOHK, Tokyo Institute of Technology (Japan)_
- Romaric Ludinard, _IMT Atlantique (France)_
- Maria Potop-Butucaru, _Sorbonne Université (France)_
- Leonardo Querzoni, _Sapienza University of Rome (Italy)_
- François Taiani, _Université Rennes 1, Irisa (France)_
- Sara Tucci-Piergiovanni, _CEA LIST (France)_
- Marko Vukolic, _IBM Research - Zurich (Switzerland)_
- Josef Widder, _Interchain Foundation & TU Wien (Austria)_

### Economics

- Bruno Biais, _HEC Paris (France)_
- Christophe Bisière, _University Toulouse Capitole, TSE and TSM-R (France)_
- Matthieu Bouvard, _University Toulouse Capitole, TSE and TSM-R  (France)_
- Catherine Casamatta, _University Toulouse Capitole, TSE and TSM-R  (France)_
- Jonathan Chiu, _Bank of Canada (Canada)_
- Will Cong, _Cornell University, Johnson Graduate School of Management (USA)_
- Guillaume Haeringer, _Baruch College, Zicklin School of Business (USA)_
- Hanna Halaburda, _New York University and Bank of Canada (USA & Canada)_
- Zhiguo He, _University of Chicago, Booth School of Business (USA)_
- Emiliano Pagnotta, _Imperial College Business School (U.K.)_
- Julien Pratt, _CNRS and CREST (France)_
- Linda Shilling, _Ecole Polytechnique and CREST (France)_
- Katrin Tinn, _McGill University, Desautels Faculty of Management (Canada)_
- David Yermack, _New York University, Stern School of Business (USA)_