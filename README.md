Website for the TOKENOMICS Conference

How to change the content ?

Either edit files directly on this website, or clone, commit, push this repository.
After each modification / push, the website is rebuilt and updated within few minutes.


- **Partners:** each file in the `content/partners` directory represents a partner. So you can modify one of the existing files, or add a new file in order to add a partner.
- **Speakers:** each file in the `content/speackers` directory represents a speacker. So you can modify one of the existing files, or add a new file in order to add a speacker.
- **Important dates:** open the file `content/_index.md` and change the dates.
- **The venue:**  open the file `content/_index.md` and change what is after `## The venue`.
- **Chairs and organisers:** change the content of the file `content/chairs_and_organisers.md` 
- **Committee:** change the content of the file `content/program_committee.md` 

To add something new in the front page, just open `content/_index.md` and add something in it!.

